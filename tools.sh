#! /bin/bash

source $HOME/.bashrc

# bin folder
mkdir -p $HOME/.local/bin
if [ -z "$(cat $HOME/.bashrc | grep /.local/bin)" ]
then
	echo "export PATH=$PATH:$HOME/.local/bin" >> $HOME/.bashrc
else
	echo ".local/bin already in your path"
fi

# kubectx
if [ -z "$(command -v kubectx)" ]
then
	cd $HOME
	git clone https://github.com/ahmetb/kubectx $HOME/kubectx
	mv $HOME/kubectx/kube* $HOME/.local/bin/.
	rm -rf $HOME/kubectx
else
	echo "kubectx already installed"
fi

# kubectl aliases
if [ -z "$(cat $HOME/.bashrc | grep /.kubectl_aliases)" ]
then
	cd $HOME
	wget https://raw.githubusercontent.com/ahmetb/kubectl-alias/master/.kubectl_aliases
	echo "[ -f ~/.kubectl_aliases ] && source ~/.kubectl_aliases" >> $HOME/.bashrc
else
	echo "kubectl_aliases already installed"
fi

# krompt
if [ -z "$(cat $HOME/.bashrc | grep K-PROMPT)" ]
then
	wget https://gitlab.com/ameer00/bashful/raw/master/krompt.txt
	cat $HOME/krompt.txt >> ~/.bashrc
	rm $HOME/krompt.txt
else
	echo "krompt already installed"
fi

# awscli
if [ -z "$(command -v aws)" ]
then
	cd $HOME
	pip3 install awscli --upgrade --user
	export PATH=$PATH:$HOME/.local/bin
else
	echo "awscli already installed"
fi

# eksctl
if [ -z "$(command -v eksctl)" ]
then
	cd $HOME
	curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
	sudo mv /tmp/eksctl $HOME/.local/bin/
else
	echo "eksctl already installed"
fi

# aws-iam-authenticator
if [ -z "$(command -v aws-iam-authenticator)" ]
then
	cd $HOME
	curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.11.5/2018-12-06/bin/linux/amd64/aws-iam-authenticator
	chmod +x ./aws-iam-authenticator
	mv ./aws-iam-authenticator $HOME/.local/bin/aws-iam-authenticator
else
	echo "aws-iam-authenticator already installed"
fi

source $HOME/.bashrc